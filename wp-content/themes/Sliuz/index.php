<?php get_header(); ?>
<div class="main">
    <div class="content">
        <h1>Main Content</h1>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <p><?php the_content(__('(more...)')); ?></p>
        <hr> <?php endwhile; ?>
        <p><?php _e('Sorry, no post'); ?></p>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>